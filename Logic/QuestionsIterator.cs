﻿using POPS.Data.DataObjects;
using System.Collections.Generic;
using System.Linq;
using System;
using POPS.Data;

namespace POPS.Logic
{
    public class QuestionsIterator
    {
        private readonly Question[] _questions;
        private  int _currentQuestionIndex;

        public QuestionsIterator(IEnumerable<Question> questions, IEnumerable<Answer> answers, GlucoseLevelType? glucoseLevelType)
        {
            var query = questions.OrderBy(q => q.Order).AsQueryable();
            if (glucoseLevelType == GlucoseLevelType.High)
                query = query.OrderBy(q => q.GlucoseTypes != null && q.GlucoseTypes.Contains(glucoseLevelType.Value) ? q.PriorityForHighGlucose : int.MaxValue);
            else if (glucoseLevelType == GlucoseLevelType.Low)
                query = query.OrderBy(q => q.GlucoseTypes != null && q.GlucoseTypes.Contains(glucoseLevelType.Value) ? q.PriorityForLowGlucose : int.MaxValue);

            _questions = query.ToArray();
            var lastAnswer = answers.OrderByDescending(a => a.CreatedOn).FirstOrDefault();
            int lastQuestionIndex = lastAnswer != null ? Array.FindIndex(_questions, q => q.Id == lastAnswer.QuestionId) : -1;

            _currentQuestionIndex = lastQuestionIndex;
        }

        public Question GetCurrentQuestion()
        {
            return _questions[_currentQuestionIndex];
        }

        public Question MoveToNextQuestion()
        {
            _currentQuestionIndex = (_questions.Length - 1) < (_currentQuestionIndex + 1) ? 0 : _currentQuestionIndex + 1;

            return _questions[_currentQuestionIndex];
        }
    }
}
