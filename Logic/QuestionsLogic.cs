﻿using POPS.Data;
using POPS.Data.DataObjects;
using POPS.Repositories;
using System;

namespace POPS.Logic
{
    public class QuestionsLogic
    {
        private readonly Settings _settings;
        private readonly AnswersRepository _answersRepository;

        public QuestionsLogic(Settings settings)
        {
            _settings = settings;
            _answersRepository = new AnswersRepository(settings);
        }

        public void Ask(string user, DateTime date, GlucoseLevelType? glucoseLevelType)
        {
            Question question = GetQuestion(user, date, glucoseLevelType);

            if (question != null)
            {
                var answer = question.Ask();
                _answersRepository.Add(new Answer
                {
                    CreatedOn = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, DateTimeKind.Utc),
                    QuestionId = question.Id,
                    User = user,
                    IsPositive = Utility.isPositiveAnswer(answer, question.PositiveAnswerPredicate),
                    QuestionType = question is WeeklyQuestion ? QuestionType.Weekly : QuestionType.Daily,
                    GlucoseLevelTypes = question.GlucoseTypes
                });
            }
               
            else
                Console.WriteLine("No more questions for today!");
        }

        private Question GetQuestion(string user, DateTime date, GlucoseLevelType? glucoseLevelType)
        {
            Question question = null;

            var builder = new QuestionsSourceBuilder(_settings, user, date, glucoseLevelType);
            foreach (QuestionType type in Enum.GetValues(typeof(QuestionType)))
            {
                var questionsSource = builder.Build(type);
                if (questionsSource != null && questionsSource.AreQuestionsAllowed(date))
                {
                    question = questionsSource.GetQuestion(date);
                    break;
                }
            }

            return question;
        }
    }
}
