﻿using POPS.Data;
using POPS.Data.DataObjects;
using POPS.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace POPS.Logic
{
    public abstract class QuestionsSource
    {
        protected readonly AnswersRepository _answersRepository;

        protected virtual QuestionType _questionType { get; }
        protected readonly GlucoseLevelType? _glucoseLevelType;

        protected QuestionsIterator _questionsIterator;
        protected List<Answer> _answers;

        public QuestionsSource(Settings settings, GlucoseLevelType? glucoseLevelType)
        {
            _answersRepository = new AnswersRepository(settings);
            _glucoseLevelType = glucoseLevelType;
        }

        private bool Validate(Question question, DateTime date)
        {
            var lastAnswer = _answers.SingleOrDefault(q => q.QuestionId == question.Id);
            if (lastAnswer == null) return true;

            return question.CanBeAsked(date, lastAnswer.CreatedOn, lastAnswer.IsPositive, _glucoseLevelType);
        }

        public virtual Question GetQuestion(DateTime? currentDate)
        {
            LoadQuestionsAndAnswers();

            var date = currentDate ?? DateTime.UtcNow;

            Question questionToBeAsked = null;
            while (questionToBeAsked == null)
            {
                var question = _questionsIterator.MoveToNextQuestion();
                
                if (Validate(question, date)) questionToBeAsked = question; 
            }

            return questionToBeAsked;
        }

        public abstract bool AreQuestionsAllowed(DateTime date);

        protected abstract void LoadQuestionsAndAnswers();
    }
}
