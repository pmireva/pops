﻿using POPS.Data;
using System;

namespace POPS.Logic
{
    public class QuestionsSourceBuilder
    {
        private readonly string _user;
        private readonly DateTime _date;
        private readonly Settings _settings;
        private readonly GlucoseLevelType? _glucoseLevelType;

        public QuestionsSourceBuilder(Settings settings, string user, DateTime date, GlucoseLevelType? glucoseLevelType)
        {
            _user = user;
            _date = date;
            _glucoseLevelType = glucoseLevelType;
            _settings = settings;
        }

        public QuestionsSource Build(QuestionType type)
        {
            if (type == QuestionType.Weekly)
                return new WeeklyQuestionsSource(_user, _settings);
            if (type == QuestionType.Daily)
                return new DailyQuestionsSource(_user, _settings, _glucoseLevelType);

            return null;
        }
    }
}
