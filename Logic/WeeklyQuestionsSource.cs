﻿using POPS.Data;
using POPS.Repositories;
using System;

namespace POPS.Logic
{
    public class WeeklyQuestionsSource : QuestionsSource
    {
        private readonly WeeklyQuestionsRepository _weeklyQuestionsRepository;

        protected override QuestionType _questionType { get { return QuestionType.Weekly; } }
        private readonly string _username;

        public WeeklyQuestionsSource(string user, Settings settings) : 
            base(settings, null)
        {
            _username = user;
            _weeklyQuestionsRepository = new WeeklyQuestionsRepository(settings);
        }

        public override bool AreQuestionsAllowed(DateTime date)
        {
            var lastAnswerDate = _answersRepository.GetLastUserAnswerByType(_username, _questionType)?.CreatedOn;
            if (lastAnswerDate == null) return true;

            DateTime start = Utility.GetSpecificDayOfWeekByDate(date, DayOfWeek.Monday);
            DateTime end = Utility.GetSpecificDayOfWeekByDate(date, DayOfWeek.Sunday);

            return !(start <= lastAnswerDate.Value && lastAnswerDate.Value <= end);
        }

        protected override void LoadQuestionsAndAnswers()
        {
            _answers = _answersRepository.GetAllLastUserAnswers(_username, _questionType, _glucoseLevelType);

            _questionsIterator = new QuestionsIterator(_weeklyQuestionsRepository.GetAll(), _answers, _glucoseLevelType);
        }
    }
}
