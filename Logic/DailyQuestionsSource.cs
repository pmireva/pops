﻿using POPS.Data;
using POPS.Repositories;
using System;

namespace POPS.Logic
{
    public class DailyQuestionsSource : QuestionsSource
    {
        private readonly DailyQuestionsRepository _dailyQuestionsRepository;

        protected override QuestionType _questionType { get { return QuestionType.Daily; } }
        private readonly string _username;

        public DailyQuestionsSource(
            string username,
            Settings settings,
            GlucoseLevelType? glucoseLevelType) :
            base(settings, glucoseLevelType)
        {
            _username = username;
            _dailyQuestionsRepository = new DailyQuestionsRepository(settings);
        }

        public override bool AreQuestionsAllowed(DateTime date)
        {
            var lastAnswerDate = _answersRepository.GetLastUserAnswerByType(_username, _questionType)?.CreatedOn;
            if (lastAnswerDate == null) return true;

            DateTime start = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, DateTimeKind.Utc);
            DateTime end = new DateTime(date.Year, date.Month, date.Day, 23, 59, 59, DateTimeKind.Utc);

            return !(start <= lastAnswerDate.Value && lastAnswerDate.Value <= end);
        }

        protected override void LoadQuestionsAndAnswers()
        {
            _answers = _answersRepository.GetAllLastUserAnswers(_username, _questionType, _glucoseLevelType);

            _questionsIterator = new QuestionsIterator(_dailyQuestionsRepository.GetAll(), _answers, _glucoseLevelType);
        }
    }
}
