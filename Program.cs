﻿using POPS.Data;
using POPS.Logic;
using POPS.Repositories;
using System;

namespace POPS
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var settings = new Settings
            {
                ConnectionString = "mongodb://localhost",
                Database = "POPS"
            };

            QuestionsLogic logic = new QuestionsLogic(settings);

            Console.WriteLine("Do you want to seed questions data? y/n");
            ConsoleKeyInfo seedDataAnswer = Console.ReadKey();
            if (seedDataAnswer.Key == ConsoleKey.Y)
            {
                var weeklyQuestionsRepo = new WeeklyQuestionsRepository(settings);
                var dailyQuestionsRepo = new DailyQuestionsRepository(settings);
                weeklyQuestionsRepo.Seed();
                dailyQuestionsRepo.Seed();
            }

            Console.WriteLine("What is your email?");
            string userAnswer = Console.ReadLine();

            ConsoleKeyInfo quitAnswer;
            while (quitAnswer.Key != ConsoleKey.Y)
            {
                Console.WriteLine("What date is it today? (MM/dd/yyyy)");
                string date = Console.ReadLine();

                Console.WriteLine("What is your glucose level? h/l/n");
                ConsoleKeyInfo glucoseAnswer = Console.ReadKey();

                logic.Ask(userAnswer, DateTime.Parse(date), Utility.mapGlucoseLevelAnswerToValue[glucoseAnswer.Key.ToString()]);

                Console.WriteLine("Do you want to quit quiz? y/n");
                quitAnswer = Console.ReadKey();

            }
            Console.WriteLine("Bye!");
            Console.ReadKey();
        }
    }
}