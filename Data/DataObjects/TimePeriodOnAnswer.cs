﻿namespace POPS.Data.DataObjects
{
    public class TimePeriodOnAnswer
    {
        public int Count { get; set; }
        public TimePeriod Type { get; set; }
    }
}
