﻿namespace POPS.Data.DataObjects
{
    public class AnswerPredicate
    {
        public string RightValue { get; set; }
        public Operators Operator { get; set; }
    }
}
