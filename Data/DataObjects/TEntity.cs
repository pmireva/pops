﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace POPS.Data.DataObjects
{
    public class TEntity
    {
        [BsonId]
        public Guid Id { get; set; }
    }
}
