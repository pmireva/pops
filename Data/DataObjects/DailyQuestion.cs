﻿using System;

namespace POPS.Data.DataObjects
{
    public class DailyQuestion : Question
    {
        public override bool CanBeAsked(DateTime currentDate, DateTime? lastAnswerDate, bool? hadPositiveAnswer, GlucoseLevelType? glucoseLevelType)
        {
            bool isWinner = true;
            if (glucoseLevelType.HasValue)
            {
                int number = new Random().Next(1, 100);

                isWinner = this.GlucoseTypes.Contains(glucoseLevelType.Value) ? number <= 75 : number >= 75;
            }
            if (!lastAnswerDate.HasValue && isWinner) return true;

            DateTime minDateToAsk = lastAnswerDate.Value.AddHours(48);
            return DateTime.Compare(minDateToAsk, currentDate) <= 0;
        }
    }
}
