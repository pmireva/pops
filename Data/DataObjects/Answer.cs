﻿using System;
using System.Collections.Generic;

namespace POPS.Data.DataObjects
{
    public class Answer : TEntity
    {
        public string User { get; set; }
        public Guid QuestionId { get; set; }
        public bool IsPositive { get; set; }
        public QuestionType QuestionType { get; set; }
        public List<GlucoseLevelType> GlucoseLevelTypes { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
