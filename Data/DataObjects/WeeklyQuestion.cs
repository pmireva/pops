﻿using System;

namespace POPS.Data.DataObjects
{
    public class WeeklyQuestion : Question
    {
        public TimePeriodOnAnswer SkipTimeOnPositiveAnswer { get; set; }
        public TimePeriodOnAnswer SkipTimeOnNegativeAnswer { get; set; }

        public override bool CanBeAsked(DateTime currentDate, DateTime? lastAnswerDate, bool? hadPositiveAnswer, GlucoseLevelType? glucoseLevelType)
        {
            if (!lastAnswerDate.HasValue) return true;

            TimePeriodOnAnswer timePeriodToSkip = hadPositiveAnswer.Value ? SkipTimeOnPositiveAnswer : SkipTimeOnNegativeAnswer;

            DateTime minDateToAsk = DateTime.UtcNow;
            if (timePeriodToSkip.Type == TimePeriod.Month)
            {
                minDateToAsk = lastAnswerDate.Value.AddMonths(timePeriodToSkip.Count);
            }
            else if (timePeriodToSkip.Type == TimePeriod.Week)
            {
                minDateToAsk = lastAnswerDate.Value.AddDays(timePeriodToSkip.Count);
            }
            return DateTime.Compare(minDateToAsk, currentDate) <= 0;
        }
    }
}
