﻿using POPS.Repositories;
using System;
using System.Collections.Generic;

namespace POPS.Data.DataObjects
{
    public class Question : TEntity
    {
        public Question()
        {
            GlucoseTypes = new List<GlucoseLevelType>();
        }

        public string Description { get; set; }
        public int Order { get; set; }
        public AnswerPredicate PositiveAnswerPredicate { get; set; }

        public List<GlucoseLevelType> GlucoseTypes { get; set; }
        public int? PriorityForLowGlucose { get; set; }
        public int? PriorityForHighGlucose { get; set; }

        public virtual bool CanBeAsked(DateTime currentDate, DateTime? lastAnswerDate, bool? hadPositiveAnswer, GlucoseLevelType? glucoseLevelType) { return true; }

        public string Ask()
        {
            Console.WriteLine("{0}", Description);
            ConsoleKeyInfo answer = Console.ReadKey();

            return answer.KeyChar.ToString();
        }
    }
}
