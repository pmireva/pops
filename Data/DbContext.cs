﻿using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace POPS.Data
{
    public class DbContext
    {
        private readonly IMongoDatabase _database;

        public DbContext(Settings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(settings.Database);

            var conventionPack = new ConventionPack { new IgnoreExtraElementsConvention(true) };
            ConventionRegistry.Register("IgnoreExtraElements", conventionPack, type => true);
        }

        public IMongoCollection<T> Set<T>()
        {
            return _database.GetCollection<T>(typeof(T).Name);
        }
    }
}
