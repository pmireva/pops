﻿namespace POPS.Data
{
    public enum TimePeriod
    {
        Day,
        Week,
        Month
    }
}
