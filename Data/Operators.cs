﻿using System.Text;

namespace POPS.Data
{
    public enum Operators
    {
        Eq,
        Gt,
        Gte,
        Lt,
        Lte
    }
}
