﻿using POPS.Data.DataObjects;
using System;
using System.Collections.Generic;

namespace POPS.Data
{
    public class Utility
    {
        private static Func<string, string, bool> eq = (left, right) => left == right;
        private static Func<int, int, bool> gt = (left, right) => left > right;
        private static Func<int, int, bool> gte = (left, right) => left >= right;
        private static Func<int, int, bool> lt = (left, right) => left < right;
        private static Func<int, int, bool> lte = (left, right) => left <= right;

        public static Dictionary<Operators, Delegate> mapOperatorToFn = new Dictionary<Operators, Delegate>
        {
            { Operators.Eq, eq },
            { Operators.Gt, gt },
            { Operators.Gte, gte },
            { Operators.Lt, lt },
            { Operators.Lte, lte }
        };

        private static Dictionary<DayOfWeek, int> mapDayToPosition = new Dictionary<DayOfWeek, int>
        {
            { DayOfWeek.Monday, 1 },
            { DayOfWeek.Tuesday, 2 },
            { DayOfWeek.Wednesday, 3 },
            { DayOfWeek.Thursday, 4 },
            { DayOfWeek.Friday, 5 },
            { DayOfWeek.Saturday, 6 },
            { DayOfWeek.Sunday, 7 }
        };

        public static Dictionary<string, GlucoseLevelType?> mapGlucoseLevelAnswerToValue = new Dictionary<string, GlucoseLevelType?>
        {
            {"H", GlucoseLevelType.High},
            {"L", GlucoseLevelType.Low},
            {"N", null}
        };

        public static DateTime GetSpecificDayOfWeekByDate(DateTime date, DayOfWeek requestedWeekDay)
        {
            int requestedDayPosition = mapDayToPosition[requestedWeekDay];
            int currentDayPosition = mapDayToPosition[date.DayOfWeek];
            DateTime requestedDate = date.AddDays(requestedDayPosition - currentDayPosition);

            return new DateTime(requestedDate.Year, requestedDate.Month, requestedDate.Day, 0, 0, 0, DateTimeKind.Utc);
        }

        public static bool isPositiveAnswer(string value, AnswerPredicate predicate)
        {
            var fn = Utility.mapOperatorToFn[predicate.Operator];
            object result = fn.DynamicInvoke(new[] { value, predicate.RightValue });

            return (bool)result;
        }
    }
}
