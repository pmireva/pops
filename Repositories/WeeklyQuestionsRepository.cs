﻿using POPS.Data;
using POPS.Data.DataObjects;

namespace POPS.Repositories
{
    public class WeeklyQuestionsRepository : Repository<WeeklyQuestion>
    {
        public WeeklyQuestionsRepository(Settings settings) : base(settings)
        {
        }

        public void Seed()
        {
            this.RemoveAll();
            string[] boolQuestions = new [] 
            {
                "Have you had a professional feet exam in the last year?",
                "Have you had an eye exam in the last year?",
                "Have you had diabetes education in the last year?",
                "Have you had a physician exam in the last year?",
                "Have you had your blood pressure checked in the last year?",
                "Have you had your cholesterol checked in the last year?",
                "Have you had an A1C test in the last year?",
                "Have you had dental exam in the last year?",
                "Have you talked to a family member recently about your diabetes control?",
                "Are you smoking?",
                "Are you taking a daily aspirin?",
                "Have you had your kidney function checked in the last year?"
            };
            int index = 0;
            foreach (var question in boolQuestions)
            {
                this.Add(new WeeklyQuestion
                {
                    Description = question,
                    Order = index++,
                    SkipTimeOnPositiveAnswer = new TimePeriodOnAnswer
                    {
                        Count = 6,
                        Type = TimePeriod.Month
                    },
                    SkipTimeOnNegativeAnswer =  new TimePeriodOnAnswer
                    {
                        Count = 6,
                        Type = TimePeriod.Week
                    },
                    PositiveAnswerPredicate = new AnswerPredicate
                    {
                        Operator = Operators.Eq,
                        RightValue = "Y"
                    }
                });
                if (index == 9) index = 13;
            }

            string[] questionsWithScaleMetric = new[]
            {
                "How are you doing with weight control?  1-5",
                "My friends are supportive of my diabetes control  1-5",
                "My family are supportive of my diabetes control  1-5",
                "Money availability is affecting my diabetes control  1-5"
            };
            int secondIndex = 9;
            foreach (var question in questionsWithScaleMetric)
            {
                this.Add(new WeeklyQuestion
                {
                    Description = question,
                    Order = secondIndex++,
                    SkipTimeOnPositiveAnswer = new TimePeriodOnAnswer
                    {
                        Count = 6,
                        Type = TimePeriod.Month
                    },
                    SkipTimeOnNegativeAnswer = new TimePeriodOnAnswer
                    {
                        Count = 6,
                        Type = TimePeriod.Week
                    },
                    PositiveAnswerPredicate = new AnswerPredicate
                    {
                        Operator = Operators.Gte,
                        RightValue = "3"
                    }
                });
            }
        }
    }
}
