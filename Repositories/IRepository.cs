﻿using MongoDB.Driver;
using POPS.Data.DataObjects;
using System;

namespace POPS.Repositories
{
    public interface IRepository<T> where T : TEntity
    {
        T Get(Guid id);
        T Add(T item);
        ReplaceOneResult Update(T item);
        T Remove(Guid id);
        DeleteResult RemoveAll();
    }
}
