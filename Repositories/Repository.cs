﻿using MongoDB.Driver;
using POPS.Data;
using POPS.Data.DataObjects;
using System;
using System.Collections.Generic;

namespace POPS.Repositories
{
    public abstract class Repository<T> : IRepository<T> where T : TEntity
    {
        protected readonly DbContext dbContext = null;

        public Repository(Settings settings)
        {
            this.dbContext = new DbContext(settings);
        }
        public List<T> GetAll()
        {
            return dbContext.Set<T>().Find(_ => true).ToList();
        }


        public T Get(Guid id)
        {
            return dbContext.Set<T>().Find(x => x.Id == id).Single();
        }

        public T Add(T item)
        {
            dbContext.Set<T>().InsertOne(item);

            return item;
        }
        public T Remove(Guid id)
        {
            var item = dbContext.Set<T>().FindOneAndDelete(b => b.Id == id);

            return item;
        }

        public ReplaceOneResult Update(T item)
        {
            return dbContext.Set<T>().ReplaceOne(n =>
                n.Id.Equals(item.Id),
                item,
                new UpdateOptions { IsUpsert = false });
        }

        public DeleteResult RemoveAll()
        {
            return dbContext.Set<T>().DeleteMany(_ => true);
        }
    }
}
