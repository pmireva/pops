﻿using POPS.Data;
using POPS.Data.DataObjects;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;

namespace POPS.Repositories
{
    public class DailyQuestionsRepository : Repository<DailyQuestion>
    {
        public DailyQuestionsRepository(Settings settings) : base(settings)
        {
        }

        public void Seed()
        {
            this.RemoveAll();
            List<Tuple<string, GlucoseLevelType[], int?, int?>> boolQuestions = new List<Tuple<string, GlucoseLevelType[], int?, int?>>
            {
                new Tuple<string, GlucoseLevelType[], int?, int?>("Do you have any sores on your feet?", null, null, null),
                new Tuple<string, GlucoseLevelType[], int?, int?>("Do you have any numbness or tingling in your fingers?", null, null, null),
                new Tuple<string, GlucoseLevelType[], int?, int?>("Do you have any numbness or tingling in your feet?", null, null, null),
                new Tuple<string, GlucoseLevelType[], int?, int?>("Have you had any low blood sugar moments in last week?", new [] {GlucoseLevelType.Low }, 1, null),
                new Tuple<string, GlucoseLevelType[], int?, int?>("Have you lost consciousness (passed out) in last month?", new [] { GlucoseLevelType.Low }, 2, null),
                new Tuple<string, GlucoseLevelType[], int?, int?>("Are you struggling to control your blood glucose?", new [] {GlucoseLevelType.High, GlucoseLevelType.Low }, 3, 4),
                new Tuple<string, GlucoseLevelType[], int?, int?>("How sure are you about how to count carbs?", new [] { GlucoseLevelType.High }, null, 5),
                new Tuple<string, GlucoseLevelType[], int?, int?>("How sure are you about how to calculate insulin amounts?",new [] { GlucoseLevelType.High }, null, 6),
                new Tuple<string, GlucoseLevelType[], int?, int?>("How are you feeling about your ability to meet diabetes goals?",new [] { GlucoseLevelType.High }, null, 7),
                new Tuple<string, GlucoseLevelType[], int?, int?>("How sure are you about choosing healthy foods?", new [] { GlucoseLevelType.High }, null, 8),
                new Tuple<string, GlucoseLevelType[], int?, int?>("How much do you feel the symptoms of low glucose levels?", new [] {GlucoseLevelType.Low }, 4, null),
                new Tuple<string, GlucoseLevelType[], int?, int?>("How sure are you about choosing healthy foods?", new [] { GlucoseLevelType.Low }, 5, null),
                new Tuple<string, GlucoseLevelType[], int?, int?>("How much do you feel the symptoms of low glucose levels?", null, null, null),
                new Tuple<string, GlucoseLevelType[], int?, int?>("Do you have a plan for when your glucose is too low?", null, null, null),
                new Tuple<string, GlucoseLevelType[], int?, int?>("Are you having any side effects from medications?", null, null, null),
                new Tuple<string, GlucoseLevelType[], int?, int?>("Do you have diarrhea?", null, null, null),
                new Tuple<string, GlucoseLevelType[], int?, int?>("Do you have constipation?", null, null, null),
                new Tuple<string, GlucoseLevelType[], int?, int?>("Have you exercised today?", null, null, null),
                new Tuple<string, GlucoseLevelType[], int?, int?>("Have you taken your diabetes medications today?", new [] { GlucoseLevelType.High }, null, 1),
                new Tuple<string, GlucoseLevelType[], int?, int?>("Have you taken your insulin today?", new [] { GlucoseLevelType.High }, null, 2),
                new Tuple<string, GlucoseLevelType[], int?, int?>("Have you checked your feet today?", null, null, null),
                new Tuple<string, GlucoseLevelType[], int?, int?>("How are you feeling today?", new [] { GlucoseLevelType.High }, null, 3),
            };
            int index = 0;
            foreach (var question in boolQuestions)
            {
                this.Add(new DailyQuestion
                {
                    Description = question.Item1,
                    Order = index++,
                    GlucoseTypes = question.Item2?.ToList(),
                    PriorityForLowGlucose = question.Item3,
                    PriorityForHighGlucose = question.Item4,
                    PositiveAnswerPredicate = new AnswerPredicate
                    {
                        Operator = Operators.Eq,
                        RightValue = "Y"
                    }
                });
                if (index == 6) index = 12;
            }

            string[] questionsWithScaleMetric = new[]
            {
                "How are you doing with weight control? 1-5",
                "My friends are supportive of my diabetes control  1-5",
                "My family are supportive of my diabetes control  1-5",
                "Money availability is affecting my diabetes control  1-5"
            };
            int secondIndex = 7;
            foreach (var question in questionsWithScaleMetric)
            {
                this.Add(new DailyQuestion
                {
                    Description = question,
                    Order = secondIndex++,
                    PositiveAnswerPredicate = new AnswerPredicate
                    {
                        Operator = Operators.Gte,
                        RightValue = "3"
                    }
                });
            }
        }
    }
}
