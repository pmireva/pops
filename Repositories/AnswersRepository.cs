﻿using MongoDB.Driver;
using POPS.Data;
using POPS.Data.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace POPS.Repositories
{
    public class AnswersRepository : Repository<Answer>
    {
        public AnswersRepository(Settings settings) : base(settings)
        {
        }
        
        public List<Answer> GetByDateRange(string username, DateTime start, DateTime end)
        {
            return dbContext.Set<Answer>()
                .FindSync(a =>
                a.User == username &&
                a.CreatedOn >= start &&
                a.CreatedOn <= end)
                .ToList();
        }

        public List<Answer> GetAllLastUserAnswers(string username, QuestionType type, GlucoseLevelType? glucoseLevelType)
        {
            var result = dbContext.Set<Answer>()
                .Find(a => a.User == username && a.QuestionType == type).ToList();

                if (glucoseLevelType.HasValue)
                {
                    result = result.Where(a => glucoseLevelType.HasValue && a.GlucoseLevelTypes != null ? a.GlucoseLevelTypes.Contains(glucoseLevelType.Value) : true).ToList();
                }
               
                return result.GroupBy(r => r.QuestionId)
                .Select(r => r.OrderBy(s => s.CreatedOn).Last())
                .ToList();
        }

        public Answer GetLastUserAnswerByType(string username, QuestionType type)
        {
            return dbContext.Set<Answer>()
                .Find(a => a.User == username && a.QuestionType == type)
                .SortByDescending(a => a.CreatedOn)
                .FirstOrDefault();
        }
    }
}
